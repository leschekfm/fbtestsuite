yaml = require('js-yaml');
fs   = require('fs');

// Get config, or throw exception on error
var config;
try {
  config = yaml.safeLoad(fs.readFileSync(__dirname + '/config.yaml', 'utf8'));
} catch (e) {
  if (e.errno === 34) {
    console.log("Couldn't find custom config file. Using default instead");
    try {
      config = yaml.safeLoad(fs.readFileSync(__dirname + '/config.default.yaml', 'utf8'));
    } catch (e) {
      console.log(e);
    }
  }
}

describe("Feed reading", function() {
  var needle = require("needle");

  it("the simple way", function(done) {
    needle.get(config.graphUrl + config.testPage + '/feed?access_token=' + config.testUserToken, function(error, response) {
      expect(error).toBe(null);
      expect(response.body.data).toBeDefined();
      done()
    });
  });
});

describe("Creating a user post on a page through the graph api", function() {
  var local = {};

  it("should allow creation", function(done) {
    var needle = require("needle");

    // create a post on the page
    var data = {
      message: "test post " + new Date().toISOString(),
      access_token: config.testUserToken
    };

    needle.post(config.graphUrl + config.testPage + '/feed', data, function(error, response) {
      expect(error).toBe(null);
      expect(response.body.error).toBeUndefined();
      expect(response.body.id).toBeDefined();
      local.postId = response.body.id;
      done()
    });
  });


  it("should be requestable", function(done) {
    var needle = require("needle");
    var url = config.graphUrl + local.postId + '?access_token=' + config.testUserToken;

    needle.get(url, function(error, response) {
      expect(error).toBe(null);
      expect(response.body.error).toBeUndefined();
      expect(response.body.id).toBe(local.postId);
      done()
    });
  });

  it("should be the first item in the feed edge", function(done) {
    var needle = require("needle");

    needle.get(config.graphUrl + config.testPage + '/feed?access_token=' + config.testUserToken, function(error, response) {
      expect(error).toBe(null);
      var data = response.body.data;
      expect(data).toBeDefined();
      expect(data[0].id).toBe(local.postId);
      done()
    });
  });

  it("should be the first item in the tagged edge", function(done) {
    var needle = require("needle");

    needle.get(config.graphUrl + config.testPage + '/tagged?access_token=' + config.testUserToken, function(error, response) {
      expect(error).toBe(null);
      var data = response.body.data;
      expect(data).toBeDefined();
      expect(data[0].id).toBe(local.postId);
      done()
    });
  });

  it("shouldn't be the first item in the posts edge", function(done) {
    var needle = require("needle");

    needle.get(config.graphUrl + config.testPage + '/posts?access_token=' + config.testUserToken, function(error, response) {
      expect(error).toBe(null);
      var data = response.body.data;
      expect(data).toBeDefined();
      if (data.length) {
        expect(data[0].id).not.toBe(local.postId);
      }
      done()
    });
  });

  it("shouldn't be the first item in the promotable_posts edge", function(done) {
    var needle = require("needle");

    needle.get(config.graphUrl + config.testPage + '/promotable_posts?access_token=' + config.testUserToken, function(error, response) {
      expect(error).toBe(null);
      var data = response.body.data;
      expect(data).toBeDefined();
      if (data.length) {
        expect(data[0].id).not.toBe(local.postId);
      }
      done()
    });
  });

  it("should be removable", function(done) {
    var needle = require("needle");
    var url = config.graphUrl + local.postId + '?access_token=' + config.testUserToken;

    needle.delete(url, null, null, function(error, response) {
      expect(error).toBe(null);
      expect(response.body.success).toBe(true);
      done()
    });
  });
});

describe("Creating an unpublished user post on a page", function() {
  it("should fail", function(done) {
    var needle = require("needle");

    // create a post on the page
    var data = {
      message: "test unpublished post " + new Date().toISOString(),
      published: false,
      access_token: config.testUserToken
    };

    needle.post(config.graphUrl + config.testPage + '/feed', data, function(error, response) {
      expect(error).toBe(null);
      expect(response.body).toEqual({
        "error": {
          "message": "(#200) Unpublished posts must be posted to a page as the page itself.",
          "type": "OAuthException",
          "code": 200
        }
      });
      done()
    });
  });
});

xdescribe("Creating a page post mentioning a page", function() {
  var local = {};

  beforeAll(function (done) {
    // get user info for posting to own feed
    var needle = require("needle");
    var url = config.graphUrl + '/me?access_token=' + config.testUserToken;

    needle.get(url, function(error, response) {
      expect(error).toBe(null);
      expect(response.body.error).toBeUndefined();
      expect(response.body.id).toBeDefined();
      local.userId = response.body.id;
      done()
    });
  });

  it("should allow creation", function(done) {
    var needle = require("needle");

    // create a post on the page
    var data = {
      message: "test mention @[" + config.testPage + "] " + new Date().toISOString(),
      access_token: config.testUserToken // todo to create a page post, a page access token is necessary
    };

    needle.post(config.graphUrl + config.testPage + '/feed', data, function(error, response) {
      expect(error).toBe(null);
      expect(response.body.error).toBeUndefined();
      expect(response.body.id).toBeDefined();
      local.postId = response.body.id;
      done()
    });
  });


  it("should be requestable", function(done) {
    var needle = require("needle");
    var url = config.graphUrl + local.postId + '?access_token=' + config.testUserToken;

    needle.get(url, function(error, response) {
      expect(error).toBe(null);
      expect(response.body.error).toBeUndefined();
      expect(response.body.id).toBe(local.postId);
      // check if page post
      expect(response.body.from.id).toEqual(config.testPage);
      // check if tags exist
      expect(response.body.message_tags).toBeDefined();
      done()
    });
  });

  it("should be the first item in the tagged edge", function(done) {
    var needle = require("needle");

    needle.get(config.graphUrl + config.testPage + '/tagged?access_token=' + config.testUserToken, function(error, response) {
      expect(error).toBe(null);
      var data = response.body.data;
      expect(data).toBeDefined();
      expect(data[0].id).toBe(local.postId);
      done()
    });
  });

  it("should be removable", function(done) {
    var needle = require("needle");
    var url = config.graphUrl + local.postId + '?access_token=' + config.testUserToken;

    needle.delete(url, null, null, function(error, response) {
      expect(error).toBe(null);
      expect(response.body.success).toBe(true);
      done()
    });
  });
});
