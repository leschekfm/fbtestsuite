# Facebook Test Suite

This project tries to test different aspects of the Facebook Graph API.

## Current Status

[ ![Codeship Status for leschekfm/fbTestSuite](https://codeship.com/projects/cd2e56b0-a3a8-0132-e233-12052818d981/status?branch=master)](https://codeship.com/projects/66083)

## Setup

npm install

use the default config or copy /spec/config.default.yaml to /spec/config.yaml and adjust the settings to your taste

npm test